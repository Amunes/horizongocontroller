<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!doctype html>
<html lang="en">
<head>
<script src="javascript/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="javascript/controller.js" type="text/javascript"></script>
<script src="javascript/bootstrap.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="style/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style/controller.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="icon" href="images/favicon.ico">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes, user-scalable=no">
<title>HorizonGo Controller</title>
</head>
<body>
	
	<div class="container-fluid controller">
		<div class="row">
    		<div class="col">
				<label type="button" class="btn btn-primary btn-lg btn-block btn-option" onclick="controller.runAction('startStopHorizon');">
					<i class="fas fa-power-off h1"></i>
				</label>
			</div>
			<div class="col">
				<label type="button" class="btn btn-primary btn-lg btn-block btn-option" onclick="controller.runAction('volumeUp');">
					<i class="fas fa-volume-up h1"></i>
				</label>
			</div>
			<div class="col">
				<label type="button" class="btn btn-primary btn-lg btn-block btn-option" onclick="controller.runAction('scrollUp');">
					<i class="fas fa-angle-up h1"></i>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<label type="button" class="btn btn-primary btn-lg btn-block btn-option" onclick="controller.runAction('showChannelList');">
					<i class="fas fa-list-ul h1"></i>
				</label>
			</div>
			<div class="col">
    			<label type="button" class="btn btn-primary btn-lg btn-block btn-option" onclick="controller.runAction('volumeDown');">
					<i class="fas fa-volume-down h1"></i>
				</label>
			</div>
    		<div class="col">
    			<label type="button" class="btn btn-primary btn-lg btn-block btn-option" onclick="controller.runAction('scrollDown');">
					<i class="fas fa-angle-down h1"></i>
				</label>
			</div>
    	</div>
    	<div class="row">
			<div class="col">
    			<label type="button" class="btn btn-primary btn-lg btn-block btn-option" onclick="controller.runAction('refresh');">
					<i class="fas fa-redo h1"></i>
				</label>
    		</div>
    		<div class="col">
    			<label type="button" class="btn btn-primary btn-lg btn-block btn-option" onclick="controller.startSearching();">
					<i class="fas fa-search h1"></i>
				</label>
    		</div>
    		<div class="col">
    			<label type="button" class="btn btn-primary btn-lg btn-block btn-option" onclick="controller.runAction('startOver');">
					<i class="fas fa-play h1"></i>
				</label>
    		</div>
    	</div>
		<div class="row">
    		<div class="col">
				<br /><br />
				<h4>Programy:</h2>
				<div class="channels-list"></div>
			</div>
		</div>
	</div>
	
	<div class="container-fluid searching">
		<div class="row">
    		<div class="col">
				<label type="button" class="btn btn-primary btn-lg btn-block btn-option btn-fullwidth" onclick="controller.stopSearching();">
					<i class="fas fa-chevron-circle-left h4">
						&nbsp;Zpět na ovladač
					</i>
				</label>
			</div>
		</div>
		<div class="row">
			&nbsp;
		</div>
		<div class="row">
			<div class="col">
				<input type="text" name="search_text" />
			</div>
		</div>
		<div class="row">
    		<div class="col">
				<label type="button" class="btn btn-primary btn-lg btn-block btn-option btn-fullwidth" onclick="controller.doSearch();">
					<i class="fas fa-search h3">
						Hledat
					</i>
				</label>
			</div>
		</div>
		<div class="row">
    		<div class="col">
				<br /><br />
				<h4>Nalezené záznamy:</h2>
				<div class="search-list"></div>
			</div>
		</div>
	</div>
	
	
	<script type="text/javascript">
		var controller = new HorizonControllerClient();
	</script>
</body>
</html>	