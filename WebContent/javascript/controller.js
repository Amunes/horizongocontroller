/**
 * HorizonGo Controller Client
 */

var HorizonControllerClient = function() {
	
	// local variables
	var _this = this;
	this.apiUrl = window.location.pathname + 'server';
	this.defaultChannelsList = JSON.parse('[{"title":"CT 1 HD"},{"title":"CT 2 HD"},{"title":"Nova HD"},{"title":"Prima HD"},{"title":"CT 24 HD"},{"title":"CT sport HD"},{"title":"CT D HD / CT art HD"},{"title":"Nova Cinema HD"},{"title":"Nova Action HD"},{"title":"Nova Gold HD"},{"title":"Nova 2 HD"},{"title":"Prima Comedy Central"},{"title":"Prima COOL HD"},{"title":"Prima ZOOM HD"},{"title":"Prima love HD"},{"title":"Prima MAX HD"},{"title":"Barrandov TV HD"},{"title":"Kino Barrandov HD"},{"title":"Barrandov Plus HD"},{"title":"RELAX HD"},{"title":"Jednotka"},{"title":"Dvojka"},{"title":"Seznam.cz TV HD"},{"title":"JOJ Family HD"},{"title":"JOJ Cinema HD"},{"title":"FilmBox"},{"title":"AMC HD"},{"title":"Prima KRIMI HD"},{"title":"HBO HD"},{"title":"HBO 2 HD"},{"title":"HBO 3 HD"},{"title":"Cinemax HD"},{"title":"Cinemax 2 HD"},{"title":"Film Europe HD"},{"title":"FilmBox Extra HD"},{"title":"FilmBox Premium HD"},{"title":"AXN"},{"title":"AXN HD"},{"title":"AXN Black"},{"title":"AXN White"},{"title":"CS mini / CS film / Horor film"},{"title":"Kino Svet"},{"title":"CS Festival HD"},{"title":"Film+ HD CZ"},{"title":"FilmBox Plus"},{"title":"FilmBox Family"},{"title":"Barrandov Family"},{"title":"TLC"},{"title":"Nova Sport 1 HD"},{"title":"Nova Sport 2 HD"},{"title":"Eurosport 1 HD"},{"title":"Eurosport 2 HD"},{"title":"Sport 1 HD"},{"title":"Sport 2 HD"},{"title":"Sport 5"},{"title":"arena sport 1 HD"},{"title":"arena sport 2"},{"title":"AutoMotorSport HD"},{"title":"Golf Channel HD"},{"title":"Extreme Sports"},{"title":"Fightbox HD"},{"title":"Fast & Fun HD"},{"title":"Docubox HD"},{"title":"WAR"},{"title":"Discovery Showcase HD"},{"title":"Animal Planet HD"},{"title":"Discovery HD"},{"title":"DTX HD"},{"title":"Discovery Science HD"},{"title":"IDX HD"},{"title":"Spektrum HD"},{"title":"History HD"},{"title":"National Geographic"},{"title":"National Geographic HD"},{"title":"Nat Geo Wild HD"},{"title":"Spektrum home"},{"title":"TV Paprika"},{"title":"CBS Reality"},{"title":"Fishing and Hunting"},{"title":"Travel Channel HD"},{"title":"Viasat Explore"},{"title":"Viasat History"},{"title":"Viasat NH HD"},{"title":"Crime and Investigation"},{"title":"Ocko"},{"title":"Ocko Star"},{"title":"Ocko Expres HD"},{"title":"Retro Music TV"},{"title":"REBEL"},{"title":"Slusnej kanal"},{"title":"MTV Live HD"},{"title":"MTV Europe"},{"title":"MTV Dance"},{"title":"MTV Rocks"},{"title":"VH 1"},{"title":"VH 1 Classic"},{"title":"Slagr 2"},{"title":"Slagr TV"},{"title":"Mezzo live HD"},{"title":"Mezzo"},{"title":"C Music"},{"title":"Power TV HD"},{"title":"Cartoon Network HD"},{"title":"Nickelodeon"},{"title":"Nick Jr."},{"title":"Disney Channel"},{"title":"Minimax"},{"title":"Megamax"},{"title":"JimJam"},{"title":"TuTy HD"},{"title":"ducktv"},{"title":"Noe TV"},{"title":"Hobby TV HD"},{"title":"Mnau TV"},{"title":"Mnam TV"},{"title":"Food Network HD"},{"title":"TV5 Monde"},{"title":"Fine Living HD"},{"title":"Fashion TV HD"},{"title":"regiony+"},{"title":"regionalnitelevize.cz"},{"title":"PRAHA TV"},{"title":"rtm+"},{"title":"TV ZAK"},{"title":"Polar TV"},{"title":"BBC World News"},{"title":"CNN"},{"title":"CNBC Europe"},{"title":"Euronews HD"},{"title":"France 24"},{"title":"Deutsche Welle"},{"title":"TA3"},{"title":"Russia Today HD"},{"title":"Channel One Russia"},{"title":"KBS World HD"},{"title":"CGTN"}]');
	
	// initialization
	renderChannelsList(this.defaultChannelsList);
	
	// public methods
    this.runAction = function(action) {
    	callServerAction(action, null, onActionFinish);
    };
    
    this.watchChannel = function(channel) {
    	$.post(this.apiUrl, {
    		action: 'watchChannel',
    		channel: channel
    	})
    };
    
    this.startSearching = function() {
    	var controllerDiv = $('div.controller');
    	var searchingDiv = $('div.searching');
    	
    	controllerDiv.css('display', 'none');
    	searchingDiv.css('display', 'block');
    	this.runAction('initSearching');
    };
    
    this.stopSearching = function(dontCallServerAction) {
    	var controllerDiv = $('div.controller');
    	var searchingDiv = $('div.searching');
    	
    	controllerDiv.css('display', 'block');
    	searchingDiv.css('display', 'none');
    	
    	if (!dontCallServerAction) {
    		this.runAction('stopSearching');
    	}
    };
    
    this.doSearch = function() {
    	var searchInput = $('input[name=search_text]');
    	var requestData = {
			search: searchInput.val()
    	}
    	
    	callServerAction('search', requestData, onActionFinish);
    };
    
    this.playFoundProgram = function(programIndex) {
    	$.post(this.apiUrl, {
    		action: 'playFoundProgram',
    		index: programIndex
    	});
    	this.stopSearching(true);
    };
    
    // private methods
    function callServerAction(action, data, callback) {
    	if (data) {
    		data['action'] = action;
    	} else {
    		data = { action: action };
    	}
    	
    	$.post(_this.apiUrl, data).done(callback);
    }
    
    function onActionFinish(data) {
    	if (data.channels) {
    		renderChannelsList(data.channels);
    	}
    	
    	if (data.searchResults) {
    		console.log('search results', data.searchResults);
    		renderSearchResults(data.searchResults);
    	}
    }
    
    function renderChannelsList(channels) {
    	var channelsDiv = $('div.channels-list');
    	channelsDiv.empty();
    	
    	channels.forEach(function(channel) {
    		if (!channel['programTitle']) {
    			channel.programTitle = channel.title;
    		}
    		if (!channel['programTimeRange']) {
    			channel.programTimeRange = '<i>Neznámé</i>';
    		}
    		channel.iconFileName = channel.title.replace(new RegExp('/', 'g'), '') + '.jpg';	//replace all "/" in channel name
    		channel.iconPath = 'images/channels/' + channel.iconFileName;
    		channelsDiv.append(
    				'<label type="button" class="btn btn-primary btn-lg btn-block btn-channel" onclick="controller.watchChannel(\'' 
    				+ channel.title + '\');">' 
    					+ '<div style="display:inline; float: left;">'
    						+ '<img src="' + channel.iconPath + '" width="50" height="20"/>'
    					+ '</div>'
    					+ '<div class="program-info">'
	    					+ '<small>' + channel.programTitle + '</small>'
	    					+ '<br />'
	    					+ '<small class="text-muted">' + channel.programTimeRange + '</small>'
    					+ '</div>'
    				+ '</label>');
    	});
    }
    
    function renderSearchResults(results) {
    	var searchResultsDiv = $('div.search-list');
    	searchResultsDiv.empty();
    	
    	results.forEach(function(result, index) {
    		searchResultsDiv.append(
				'<label type="button" class="btn btn-primary btn-lg btn-block btn-channel" onclick="controller.playFoundProgram(' + index + ');">' 
					+ '<div style="display:inline; float: left;">'
						+ '<img src="' + result.iconUrl + '" width="30" height="40"/>'
					+ '</div>'
					+ '<div class="program-info">'
    					+ '<small>' + result.title + '</small>'
    					+ '<br />'
    					+ '<small class="text-muted">' + result.description + '</small>'
					+ '</div>'
				+ '</label>');
    	});
    }
}