package server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Servlet implementation class ControllerServlet
 */
@WebServlet("/server")
public class ServerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * Default constructor. 
     */
    public ServerServlet() {
        // empty constructor
    }
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		ChromeController chrome = ChromeController.getInstance();
		boolean success = true;
		Map<String, Object> responseObject = new HashMap<>();
		List<Map<String, String>> channels = null;
		
		// perform action
		System.out.println("Performing action: " + action);
		if ("startStopHorizon".equals(action)) {
			channels = chrome.startStopHorizon();
		}
		else if ("showChannelList".equals(action)) {
			channels = chrome.showChannelList();
		}
		else if ("scrollUp".equals(action)) {
			chrome.scrollUp();
		}
		else if ("scrollDown".equals(action)) {
			chrome.scrollDown();
		}
		else if ("watchChannel".equals(action)) {
			String channel = request.getParameter("channel");
			chrome.watchChannel(channel);
		}
		else if ("refresh".equals(action)) {
			chrome.refresh();
		}
		else if ("volumeUp".equals(action)) {
			chrome.volumeUp();
		}
		else if ("volumeDown".equals(action)) {
			chrome.volumeDown();
		}
		else if ("initSearching".equals(action)) {
			chrome.initSearching();		
		}
		else if ("search".equals(action)) {
			String searchingText = request.getParameter("search");
			List<Map<String, String>> results = chrome.search(searchingText);
			responseObject.put("searchResults", results);
		}
		else if ("stopSearching".equals(action)) {
			chrome.stopSearching();
		}
		else if ("playFoundProgram".equals(action)) {
			String programIndex = request.getParameter("index");
			chrome.playFoundProgram(programIndex);
		}
		else if ("startOver".equals(action)) {
			chrome.startOver();
		} else {
			System.out.println("Action '" + action + "'not found");
			success = false;
		}
		
		// prepare response
		response.setContentType("application/json; charset=utf-8");
		responseObject.put("success", success);
		if (channels != null) {
			responseObject.put("channels", channels);
		}
		
		// generate JSON
		String responseJson = null;
		try {
			responseJson = new ObjectMapper().writeValueAsString(responseObject);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		// send response
		System.out.println("Sending response... " + responseJson);
		PrintWriter out = response.getWriter();
		out.println(responseJson);
		out.flush();
	}

}
