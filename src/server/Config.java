package server;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class Config {

	public static String WEB_DRIVER_PROPERTY = "chromedriver_path";
	public static String HORIZON_USERNAME_PROPERTY = "horizon_username";
	public static String HORIZON_PASSWORD_PROPERTY = "horizon_password";
	public static String HORIZON_URL = "https://www.horizon.tv/";
	public static String LANGUAGE_PREFIX = "cs_cz";
	public static String START_PAGE = ".html"; // HORIZON_URL + LANGUAGE_PREFIX + .html
	public static String TV_PAGE = "/tv.html";
	public static String SEARCHING_PAGE = "searching";
	
	/***************** APPLICATION HTML ELEMENTS CONFIGURATION *********************/
	
	public static String HTML_CONTENT_CLASS = "content";
	public static String HTML_CHANNEL_DIV_CLASS = "live-channel-tile";
	public static String VOLUME_SETTINGS_BUTTON_XPATH = "//div[@class='hrz-player-controls-top-bar-right-buttons']"
														+ "/div[contains(@class, 'hrz-player-controls-volume')]";
	public static String VOLUME_CHANGER_DIV_CLASS = "hrz-player-controls-volume-popup-marker";
	public static String OVERLAY_DIV_CLASS = "hrz-player-controls-overlay";
	public static String SEARCH_BUTTON_XPATH = "//div[@class='utility-bar-right']/a[@class='utility-bar-button']";
	public static String SEARCH_INPUT_CLASS = "search-text-input__input";
	public static String SEARCH_RESULTS_XPATH = "//div[contains(@class, 'search-ceil')]";
	public static String SEARCH_RESULT_XPATH = "//div[@class='search-cell-description']";
	public static String PLAY_PROGRAM_BUTTON_XPATH = "//div[contains(@class, 'button-with-options-select-wrapper')]/button";
	
	private static Config instance;
	private Properties properties;
	private String configFileName = "Config.properties";
	
	private Config() {
		try {
			
			//load a properties file from class path
			InputStream propertiesInput = Config.class.getClassLoader().getResourceAsStream(configFileName);
			if(propertiesInput == null){
				System.out.println("Sorry, unable to load Config.properties");
				return;
			}
			
			properties = new Properties();
			properties.load(propertiesInput);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public static Config getInstance() {
		if (instance == null) {
			instance = new Config();
		}
		
		return instance;
	}
	
	public String getProperty(String property) {
		return properties.getProperty(property);
	}
	
}
