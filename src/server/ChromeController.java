package server;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Coordinates;

public class ChromeController {
	
	private static ChromeController instance = null;
	private ChromeDriver chrome;
	private static String WEB_DRIVER_PROPERTY = "webdriver.chrome.driver";
	
	private Timer scrollTimer = new Timer();
	private boolean isScrolling = false;
	private String lastPage = null;
	private Actions actionsBuilder;
	
	//*********************** initialization ***********************
	public ChromeController() {
		if (System.getProperty(WEB_DRIVER_PROPERTY) == null) {
			initializeChromeDriver();
		}
	}
	
	private void initializeChromeDriver() {
		String webDriverPath = Config.getInstance().getProperty(Config.WEB_DRIVER_PROPERTY);
		File webDriverConfig = new File(webDriverPath);
		if (!webDriverConfig.exists()) {
			URL webDriverPathResources = Config.class.getClassLoader().getResource("chromedriver.exe");
			if (webDriverPathResources != null) {
				try {
					webDriverPath = URLDecoder.decode(webDriverPathResources.toString().replaceAll("file:/", ""),
													  StandardCharsets.UTF_8.name());
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.setProperty(WEB_DRIVER_PROPERTY, webDriverPath);
	}
	
	public static ChromeController getInstance() {
		if (instance == null) {
			instance = new ChromeController(); 
		}
		return instance;
	}
	
	// *********************** public methods ***********************
	public List<Map<String, String>> startStopHorizon() {
		if (isHorizonRunning()) {
			closeHorizon();
			return null;
		}
		
		System.out.println("Starting HorizonGo...");
		this.chrome = createChromeDriver();
		this.actionsBuilder = new Actions(chrome);
		openPage(Config.START_PAGE);
		sleep(2000);
		login();
		sleep(2500);
		return showChannelList();
	}
	
	public void closeHorizon() {
		System.out.println("Closing HorizonGo...");
		chrome.quit();
	}
	
	public List<Map<String, String>> showChannelList() {
		openPage(Config.TV_PAGE);
		sleep(1500);
		
		return loadChannelList();
	}
	
	public void scrollUp() {
		scrollPage(true);
	}
	
	public void scrollDown() {
		scrollPage(false);
	}
	
	public void refresh() {
		chrome.navigate().refresh();
	}
	
	public void watchChannel(String channel) {
		if (lastPage != null && lastPage.equals(channel)) {
			return;
		}
		if (!Config.TV_PAGE.equals(lastPage)) {
			openPage(Config.TV_PAGE);
			sleep(3000);
		}
		
		System.out.println("Starting channel " + channel);
		try {
			WebElement channelElement = chrome.findElementByXPath("//div[contains(@class, 'live-channel-tile') and @title='" + channel + "']");
			channelElement.click();
			lastPage = channel;
		} catch (Exception e) {
			// TODO: cycle count check
			watchChannel(channel);
		}
	}
	
	public void volumeUp() {
		changeVolume(20);
	}
	
	public void volumeDown() {
		changeVolume(-20);
	}
	
	public void initSearching() {
		if (Config.SEARCHING_PAGE.equals(lastPage)) {
			return;
		}
		
		try {
			WebElement searchBtn = chrome.findElementByXPath(Config.SEARCH_BUTTON_XPATH);
			searchBtn.click();
			lastPage = Config.SEARCHING_PAGE;
		} catch (Exception e) {
			openPage(Config.TV_PAGE);
			sleep(3000);
			
			// try it again
			initSearching();
		}
	}
	
	public void stopSearching() {
		try {
			WebElement closeBtn = chrome.findElementByClassName("search__close");
			closeBtn.click();
			lastPage = null;
		} catch(Exception e) {
			openPage(Config.TV_PAGE);
		}
	}
	
	
	public List<Map<String, String>> search(String searchingText) {
		WebElement searchInput = chrome.findElementByClassName(Config.SEARCH_INPUT_CLASS);
		
		if (!searchInput.isDisplayed()) {
			initSearching();
			
			// find search input again
			searchInput = chrome.findElementByClassName(Config.SEARCH_INPUT_CLASS);
		}
		
		// delete actual value of input
		chrome.executeScript("document.getElementsByClassName('" + Config.SEARCH_INPUT_CLASS + "')[0].value = '';");
		
		// set new value
		searchInput.click();
		searchInput.sendKeys(searchingText);
		sleep(1500);
		
		// load results
		return loadSearchResultsList();
	}
	
	public void playFoundProgram(String programIndex) {
		WebElement resultElement = chrome.findElementByXPath("(" + Config.SEARCH_RESULTS_XPATH + ")[" + (Integer.valueOf(programIndex) + 1) + "]");
		resultElement.click();
		sleep(1500);
		
		WebElement playBtn = chrome.findElementByXPath(Config.PLAY_PROGRAM_BUTTON_XPATH);
		playBtn.click();
		lastPage = null;
	}
	
	public void startOver() {
		WebElement startOverBtn = chrome.findElementByClassName("start-over");
		this.actionsBuilder.moveByOffset(5, 5)
						   .pause(200)
						   .click(startOverBtn)
						   .build()
						   .perform();
		lastPage = null;
	}
	
	// *********************** private methods ***********************
	
	private ChromeDriver createChromeDriver() {
		ChromeOptions options = new ChromeOptions();
	    options.addArguments("--start-fullscreen");
	    options.addArguments("disable-infobars");
		return new ChromeDriver(options);
	}
	
	private void changeVolume(int volumeChange) {
		WebElement volumeBtn = chrome.findElementByXPath(Config.VOLUME_SETTINGS_BUTTON_XPATH);
		WebElement volumeUpBtn = chrome.findElementByClassName(Config.VOLUME_CHANGER_DIV_CLASS);
		
		if (!volumeUpBtn.isDisplayed()) {
			this.actionsBuilder.moveByOffset(5, 5)
							   .pause(200)
							   .moveToElement(volumeBtn)
							   .pause(300);
		}
		
		this.actionsBuilder.clickAndHold(volumeUpBtn)
						   .moveByOffset(volumeChange, 0)
						   .release()
						   .build()
						   .perform();
	}
	
	private List<Map<String, String>> loadSearchResultsList() {
		List<Map<String, String>> results = new ArrayList<>();
		List<WebElement> resultElements = chrome.findElementsByXPath(Config.SEARCH_RESULTS_XPATH);
		String title;
		String description;
		String iconUrl;
		
		
		for (WebElement result : resultElements) {
			title = result.findElement(By.className("search-cell-description__title")).getText();
			description = result.findElement(By.className("search-cell-description__description")).getText();
			iconUrl = result.findElement(By.className("tv-program__image")).getAttribute("src");
			
			Map<String, String> resultObject = new HashMap<>();
			resultObject.put("title", title);
			resultObject.put("description", description);
			resultObject.put("iconUrl", iconUrl);
			
			results.add(resultObject);
			
		}
		
		return results;
	}
	
	/*private List<Map<String, String>> loadChannelList() {
		System.out.println("Loading channel list...");
		String channelListPageUrl = Config.HORIZON_URL + Config.LANGUAGE_PREFIX + Config.TV_PAGE;
		String jsAjaxScript = "var callback = arguments[arguments.length - 1];"
			+ "var xhttp = new XMLHttpRequest();"
			+ "xhttp.onreadystatechange = function() {"
			  + "if (this.readyState == 4 && this.status == 200) {"
			    + "var parser = new DOMParser();"
			    + "var doc = parser.parseFromString(this.responseText, \"text/html\");"
			    + "var content = doc.getElementsByClassName(\"content\")[0];"
			    + "callback(content);"
			  + "}"
			+ "};"
			+ "xhttp.open(\"GET\", \"" + channelListPageUrl + "\", true);"
			+ "xhttp.send();";
		
		WebElement channelListPage = (WebElement) chrome.executeAsyncScript(jsAjaxScript);
		List<Map<String, String>> channelsList = new ArrayList<>();
		List<WebElement> channels = channelListPage.findElements(By.className(Config.HTML_CHANNEL_DIV_CLASS));
		String title;
		String programTitle;
		String programTimeRange;
		
		for (WebElement channel : channels) {
			title = channel.getAttribute("title");
			programTitle = channel.findElement(By.className("program-title")).getText();
			programTimeRange = channel.findElement(By.className("program-time-range")).getText();
			
			//getChannelIconUrl(title); //download channel icon
			
			Map<String, String> channelObject = new HashMap<>();
			channelObject.put("title", title);
			channelObject.put("programTitle", programTitle);
			channelObject.put("programTimeRange", programTimeRange);
			
			channelsList.add(channelObject);
		}
		return channelsList;
	}*/
	
	private List<Map<String, String>> loadChannelList() {
		System.out.println("Loading channel list...");
		List<Map<String, String>> channelsList = new ArrayList<>();
		List<WebElement> channels = chrome.findElementsByClassName(Config.HTML_CHANNEL_DIV_CLASS);
		String title;
		String programTitle;
		String programTimeRange;
		
		for (WebElement channel : channels) {
			title = channel.getAttribute("title");
			programTitle = channel.findElement(By.className("program-title")).getText();
			programTimeRange = channel.findElement(By.className("program-time-range")).getText();
			
			//getChannelIconUrl(title); //download channel icon
			
			Map<String, String> channelObject = new HashMap<>();
			channelObject.put("title", title);
			channelObject.put("programTitle", programTitle);
			channelObject.put("programTimeRange", programTimeRange);
			
			channelsList.add(channelObject);
		}
		return channelsList;
	}
	
	private void getChannelIconUrl(String channelTitle) {
		String programIconUrl = null;
		Path iconFilePath = Paths.get(channelTitle.replace("/", "") + ".jpg");
		System.out.println(iconFilePath.toFile().getAbsolutePath());
		if (iconFilePath.toFile().exists()) {
			return;
		}
		try {
			System.out.println("loading channel icon..." + channelTitle);
			programIconUrl = chrome.findElementByXPath(
					"//div[contains(@class, '" + Config.HTML_CHANNEL_DIV_CLASS + "')"
					+ " and @title='" + channelTitle + "']" 
							+ "//img[contains(@class, 'orion-image')]").getAttribute("src");
			
			try(InputStream in = new URL(programIconUrl).openStream()) {
				Files.copy(in, iconFilePath);
			}
		} catch (Exception e) {
			System.out.println("loading channel icon failed - trying again..." + channelTitle);
			sleep(1000);
			getChannelIconUrl(channelTitle);
		}
	}
	
	private void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	private void login() {
		System.out.println("Logging in...");
		chrome.findElementByXPath("//div[@class='notifications-manager__item']").click();
		chrome.findElementByXPath("//span[@class='utility-bar-button-title' and text()='P�ihl�sit']").click();
		chrome.getKeyboard().sendKeys(Config.getInstance().getProperty(Config.HORIZON_USERNAME_PROPERTY));
		chrome.findElementByXPath("//input[@type='password']").click();
		chrome.getKeyboard().sendKeys(Config.getInstance().getProperty(Config.HORIZON_PASSWORD_PROPERTY));
		chrome.findElementByXPath("//span[@class='checkmark-wrapper']").click();
		chrome.findElementByXPath("//button[contains(@class, 'login-form-button')]").click();
	}
	
	private void scrollPage(boolean up) {
		if (!isHorizonRunning()) {
			startStopHorizon();
		}
		
		JavascriptExecutor js = (JavascriptExecutor) chrome;
		if (isScrolling) {
			stopScrolling();
		} else {
			// scroll by 10px every 50ms
			isScrolling = true;
			scrollTimer = new Timer();
			scrollTimer.scheduleAtFixedRate(new TimerTask() {
			   public void run() {
				   int scrollByPixel = 10;
				   if (up) {
					   scrollByPixel *= -1;
				   }
				   js.executeScript(
					   	"document.getElementsByClassName('" 
				   				+ Config.HTML_CONTENT_CLASS + "')[0].scrollBy(0, " + scrollByPixel + ")");
			   }
			}, 0, 50);
		}
	}
	
	private void stopScrolling() {
		scrollTimer.cancel();
		isScrolling = false;
	}
	
	private boolean isHorizonRunning() {
		return chrome != null && chrome.getSessionId() != null;
	}
	
	private void openPage(String page) {
		lastPage = page;
		if (!isHorizonRunning()) {
			startStopHorizon();
		}
		chrome.get(Config.HORIZON_URL + Config.LANGUAGE_PREFIX + page);
		
	}
	
}
